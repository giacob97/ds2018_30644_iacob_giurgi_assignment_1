<%--
  Created by IntelliJ IDEA.
  User: Iacob
  Date: 10/26/2018
  Time: 5:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        * {
            box-sizing: border-box;
        }

        /* Create two equal columns that floats next to each other */
        .column {
            float: left;
            width: 50%;
            padding: 10px;
            height: 200px; /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
    <title>Admin Page</title>
</head>
<body>
<form action="/admin" method="post">
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <div class="column">
            <h2>Add/Update a flight</h2>
            Flight number: <input type="text" name="flight_number">
            <br/>
            Departure city: <input type="text" name="departure_city"/>
            <br/>
            Departure date: <input type="text" name="departure_date">
            <br/>
            Arrival city: <input type="text" name="arrival_city"/>
            <br/>
            Arrival date: <input type="text" name="arrival_date"/>
            <br/>
            <br/>
            <input type="submit" name="add" value="Submit"/>
        </div>
        <div class="column">
            <h2>Remove a flight</h2>
            Flight id: <input type="text" name="id">
            <input type="submit" name="remove" value="Remove"/>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <input type="submit" name="logout" value="Log out"/>
</form>
</body>
</html>
