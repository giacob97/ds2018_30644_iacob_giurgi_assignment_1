package business;

import dao.FlightDAO;
import entity.Flight;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("all")
@WebServlet("/admin")
@WebFilter(filterName = "FilterAdmin", urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet implements Filter{


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        FlightDAO flightDAO = new FlightDAO();
        List<Flight> flights = flightDAO.findAllFlights();
        out.println("<h2>" + "All flights" + "</h2>");
        out.println("<TABLE BORDER><TR><TH>Index<TH>Flight Number<TH>Departure city<TH>Departure date<TH>Destination<TH>Arrival time</TR>");
        for (Flight flight : flights) {
            out.println("<TR><TD>" + flight.getIdFlight() + "<TD>" + flight.getFlightNumber() + "<TD>" + flight.getDepartureCity() + "<TD>" + flight.getDepartureDate()
                    + "<TD>" + flight.getArrivalCity() + "<TD>" + flight.getArrivalDate());
        }
        out.println("</TABLE>");
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/admin.jsp");
        rd.include(request, response);
        out.close();
    }


    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        FlightDAO flightDAO = new FlightDAO();
        if (request.getParameter("add") != null) {
            String flightNumber = (String) request.getParameter("flight_number");
            String departureCity = (String) request.getParameter("departure_city");
            String arrivalCity = (String) request.getParameter("arrival_city");
            String depDate =(String) request.getParameter("departure_date");
            Date departureDate = this.convertDateFromString(depDate);
            Date arrivalDate = this.convertDateFromString((String) request.getParameter("arrival_date"));
            Flight flight = new Flight(Integer.valueOf(flightNumber),departureCity,departureDate,arrivalCity,arrivalDate);
            int idFlight = flightDAO.findFlightAfterFlightNumber(Integer.valueOf(flightNumber));
            if(idFlight > -1){
                flight.setIdFlight(idFlight);
                flightDAO.updateFlight(flight);
            }
            else{
                flightDAO.addFlight(flight);
            }
            doGet(request,response);
        }
        if (request.getParameter("remove") != null) {
            String idFlight = (String) request.getParameter("id");
            flightDAO.deleteFlight(Integer.valueOf(idFlight));
            doGet(request,response);
        }
        if (request.getParameter("logout") != null) {
            request.getSession().invalidate();
            response.sendRedirect("/login");
        }
    }


    private Date convertDateFromString(String stringDate){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
        Date date = null;
        try {
            date = (Date)formatter.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");
        PrintWriter out = response.getWriter();
        if (username == null || !role.equals("admin")) {
            out.println(
                    "<html>\n" +
                            "<head><title>" + "ERROR 404" + "</title></head>\n" +
                            "<body><h1>" +
                            "ERROR 404:PAGE NOT FOUND" +
                            "</h1>" +
                            "</body>" +
                            "</html>");
        }else{
            filterChain.doFilter(servletRequest,servletResponse);
        }
    }
}
