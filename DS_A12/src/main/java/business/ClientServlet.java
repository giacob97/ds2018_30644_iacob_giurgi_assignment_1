package business;


import dao.CityDAO;
import dao.FlightDAO;

import static dao.Utils.getCurrentHour;

import entity.Flight;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("all")
@WebServlet("/client")
@WebFilter(filterName = "FilterClient", urlPatterns = {"/client"})
public class ClientServlet extends HttpServlet implements Filter {

    List<Flight> flights;
    String currentCity;

    @Override
    public void init() throws ServletException {
        this.currentCity = "Cluj-Napoca";
        this.flights = new FlightDAO().findAllFlights();

    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");
        CityDAO cityDAO = new CityDAO();
        Date date = getCurrentHour(cityDAO.getCityByName(currentCity));
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String localTime = formatter.format(date);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h2>" + "All flights" + "</h2>");
        out.println("<TABLE BORDER><TR><TH>Flight Number<TH>Departure city<TH>Departure date<TH>Destination<TH>Arrival time</TR>");
        for (Flight flight : this.flights) {
            out.println("<TR><TD>" + flight.getFlightNumber() + "<TD>" + flight.getDepartureCity() + "<TD>" + flight.getDepartureDate()
                    + "<TD>" + flight.getArrivalCity() + "<TD>" + flight.getArrivalDate());
        }
        out.println("</TABLE>");
        out.print("<h3>" + "Local time for " + this.currentCity + " is: " + localTime + "</h3>");
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/client.jsp");
        rd.include(request, response);

        out.close();

    }

    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        if (request.getParameter("logout") != null) {
            request.getSession().invalidate();
            response.sendRedirect("/login");
        }
        if (request.getParameter("submit") != null) {
            if (request.getParameter("city") != null) {
                this.currentCity = request.getParameter("city");
                doGet(request,response);
            }
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");
        PrintWriter out = response.getWriter();
        if (username == null || !role.equals("client")) {
            out.println(
                    "<html>\n" +
                            "<head><title>" + "ERROR 404" + "</title></head>\n" +
                            "<body><h1>" +
                            "ERROR 404:PAGE NOT FOUND" +
                            "</h1>" +
                            "</body>" +
                            "</html>");
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
