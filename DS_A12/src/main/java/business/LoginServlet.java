package business;

import dao.ClientDAO;
import entity.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {


    private String username;


    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        this.username = request.getParameter("username");
        String password = request.getParameter("password");
        ClientDAO clientDAO = new ClientDAO();
        Client client = clientDAO.findClientByUsername(this.username);
        if (client != null) {
            if(clientDAO.login(client,password)){
               if(client.getRole()==0){
                   HttpSession session = request.getSession(true);
                   session.setAttribute("username",client.getUsername());
                   session.setAttribute("role","client");
                   response.sendRedirect("/client");

               }
                if(client.getRole()==1){
                    HttpSession session = request.getSession(true);
                    session.setAttribute("username",client.getUsername());
                    session.setAttribute("role","admin");
                    response.sendRedirect("/admin");
                }
            }else{
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Password incorrect');");
                out.println("location='login';");
                out.println("</script>");
                doGet(request,response);
            }
        }else{
            out.println("<script type=\"text/javascript\">");
            out.println("alert('User incorrect');");
            out.println("</script>");
            doGet(request,response);
        }

    }

    @Override
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
        rd.include(request, response);


    }
}
