package entity;

import java.util.Date;

public class Flight {
    private int idFlight;
    private int flightNumber;
    private String departureCity;
    private Date departureDate;
    private String arrivalCity;
    private Date arrivalDate;


    public Flight(int flightNumber, String departureCity, Date departureDate, String arrivalCity, Date arrivalDate) {
        this.flightNumber = flightNumber;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
    }

    public Flight() {
    }

    public int getIdFlight() {
        return idFlight;
    }

    public void setIdFlight(int idFlight) {
        this.idFlight = idFlight;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }


    @Override
    public String toString() {
        return "Flight{" +
                "idFlight=" + idFlight +
                ", flightNumber=" + flightNumber +
                ", departureCity='" + departureCity + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalDate=" + arrivalDate +
                '}';
    }
}
