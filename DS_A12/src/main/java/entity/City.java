package entity;

public class City {

    private int idCity;
    private float latitude;
    private float longitude;
    private String name;

    public City(float latitude,float longitude,String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public City() {
    }

    public int getIdCity() {
        return idCity;
    }

    public void setIdCity(int idCity) {
        this.idCity = idCity;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
