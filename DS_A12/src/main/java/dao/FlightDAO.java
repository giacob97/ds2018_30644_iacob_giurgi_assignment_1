package dao;

import entity.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;


import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("all")
public class FlightDAO implements FlightDAOInterface {

    private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

    private SessionFactory factory;

    public FlightDAO() {

    }

    @Override
    public Flight addFlight(Flight flight) {
        factory = new Configuration().configure().buildSessionFactory();
        int flightId = -1;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setIdFlight(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    @Override
    public Flight findFlight(int idFlight) {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Flight flight = (Flight) session.get(Flight.class,idFlight);
        session.close();
        return flight;
    }

    @Override
    public Flight deleteFlight(int idFlight) {
        Flight flight = this.findFlight(idFlight);
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    @Override
    public Flight updateFlight(Flight flight) {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    @Override
    public List<Flight> findAllFlights() {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    @Override
    public int findFlightAfterFlightNumber(int flightNumber) {
        List<Flight> flights = this.findAllFlights();
        for(Flight flight:flights){
            System.out.println(flight.getIdFlight());
            if(flight.getFlightNumber() == flightNumber){
                return flight.getIdFlight();
            }
        }
        return -1;
    }
}
