package dao;

import entity.Client;

public interface ClientDAOInterface {
    Client addClient(Client client);
    Client findClient(int idClient);
    Client findClientByUsername(String username);
    boolean login(Client client,String password);
}
