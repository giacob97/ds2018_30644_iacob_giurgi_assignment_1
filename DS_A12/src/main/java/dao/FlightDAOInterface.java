package dao;

import entity.Flight;

import java.util.List;

public interface FlightDAOInterface {
    Flight addFlight(Flight flight);
    Flight findFlight(int idFlight);
    Flight deleteFlight(int idFlight);
    Flight updateFlight(Flight flight);
    List<Flight> findAllFlights();
    int findFlightAfterFlightNumber(int flightNumber);

}
