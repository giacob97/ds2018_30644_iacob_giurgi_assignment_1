package dao;

import entity.Client;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class ClientDAO implements ClientDAOInterface {

    private static final Log LOGGER = LogFactory.getLog(ClientDAO.class);

    private SessionFactory factory;

    public ClientDAO() {

    }

    @Override
    public Client addClient(Client client) {
        factory = new Configuration().configure().buildSessionFactory();
        int clientId = -1;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            clientId = (Integer) session.save(client);
            client.setIdClient(clientId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return client;
    }

    @Override
    public Client findClient(int idClient) {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<Client> clients = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Client WHERE id = :id");
            query.setParameter("id", idClient);
            clients = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return clients != null && !clients.isEmpty() ? clients.get(0) : null;
    }

    @Override
    public Client findClientByUsername(String username) {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<Client> clients = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Client WHERE username = :username");
            query.setParameter("username", username);
            clients = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
            factory.close();
        }
        return clients != null && !clients.isEmpty() ? clients.get(0) : null;
    }

    @Override
    public boolean login(Client client, String password) {
        return client.getPassword().equals(password);
    }


}
