package dao;

import entity.City;
import entity.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CityDAO implements CityDAOInterface {
    private static final Log LOGGER = LogFactory.getLog(CityDAO.class);

    private SessionFactory factory;
    public CityDAO() {

    }

    @Override
    public City addCity(City city) {
        int cityId = -1;
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            cityId = (Integer) session.save(city);
            city.setIdCity(cityId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return city;
    }

    @Override
    public City getCityByName(String name) {
        factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City");
            cities = query.list();
            tx.commit();
            return cities.stream().filter(x->x.getName().equals(name)).findFirst().get();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return null;
    }

}
