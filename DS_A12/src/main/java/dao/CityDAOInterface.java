package dao;

import entity.City;
import entity.Flight;

import java.util.Map;
import java.util.List;

public interface CityDAOInterface {
    City addCity(City city);
    City getCityByName(String name);
}
