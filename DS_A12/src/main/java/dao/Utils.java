package dao;

import entity.City;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class Utils {
    public static Date getCurrentHour(City city) {
        String urlString = "http://new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude()
                + "?fbclid=IwAR23sW0h6AhnXdeoKXj4EZgVeIAzCRNm0h3sULJDeN4KqoucGqwS5gejQEI";
        try {
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(urlConnection.getInputStream());
            doc.getDocumentElement().normalize();
            String stringDate  = doc.getElementsByTagName("localtime").item(0).getTextContent();
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
            return formatter.parse(stringDate);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
