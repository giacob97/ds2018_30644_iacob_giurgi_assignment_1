-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema a1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema a1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `a1` DEFAULT CHARACTER SET utf8 ;
USE `a1` ;

-- -----------------------------------------------------
-- Table `a1`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `a1`.`city` (
  `idcity` INT(11) NOT NULL,
  `latitude` FLOAT NULL DEFAULT NULL,
  `longitude` FLOAT NULL DEFAULT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `a1`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `a1`.`client` (
  `idclient` INT(11) NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(45) NULL DEFAULT NULL,
  `lastname` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `role` INT(11) NULL DEFAULT NULL,
  `username` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idclient`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `a1`.`flight`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `a1`.`flight` (
  `idflight` INT(11) NOT NULL AUTO_INCREMENT,
  `flightNumber` INT(11) NULL DEFAULT NULL,
  `departureCity` VARCHAR(45) NOT NULL,
  `departureDate` DATETIME NULL DEFAULT NULL,
  `arrivalCity` VARCHAR(45) NOT NULL,
  `arrivalDate` DATETIME NULL,
  PRIMARY KEY (`idflight`, `departureCity`, `arrivalCity`),
    FOREIGN KEY (`departureCity`)
    REFERENCES `a1`.`city` (`name`),
    FOREIGN KEY (`arrivalCity`)
    REFERENCES `a1`.`city` (`name`)
)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
